package org.suffieldacademy.lego.introcs;


import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.port.MotorPort;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * A program that makes a robot move an exact distance.  You need to
 * modify this program to intialize one or more motors and run them
 * so that the robot moves the requested distance.
 *
 * @author Your Name Here
 */
public class DistanceBot {

    /**
     * Distance the robot should travel.  Once your robot is tested
     * and working, this should be the ONLY line of code you need
     * to change for the actual run.  The rest of your code should
     * use this variable in its calculations for running the motors.
     */
    public static final int DISTANCE_IN_MM = 1000;

    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Set the escape key to quit immediately as a failsafe
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });


        // YOUR CODE HERE

        // You'll need to declare and initialize at least one motor
        // variable, then tell that motor to move.  The amount/time
        // the motor moves must be based off of DISTANCE_IN_MM defined
        // above.  You can make any other variables you want, print things
        // to the screen, etc.

    }


} // end class
