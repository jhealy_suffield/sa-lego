package org.suffieldacademy.lego.introcs;


import org.suffieldacademy.lego.introcs.LineSocket;

import java.io.IOException;

import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.port.MotorPort;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import ev3dev.actuators.ev3.EV3Led;

import lejos.utility.Delay;


/**
 * <p>
 * A skeleton class for a robot that can communicate with another
 * computer or robot using a network connection (most likely via Wi-Fi).
 * </p>
 *
 * <p>
 * This class contains all the code necessary to wait for inbound network
 * connections, accept them, and then hand off processing to a method that
 * you must complete.  It must be customized to actually send and receive
 * values from another program and act on the messages it sends and receives.
 * </p>
 *
 * <p>
 * Typically, this class must be tested with another program that uses the
 * LineSocket class for communication.  See the <b>ComputerClient</b> class
 * for an example.
 * </p>
 *
 * @author Your Name
 *
 */
public class RemoteBot {

    /** What port number to listen on (should be between 1024 and 65535).
     * The number is not too meaningful, but the other program that
     * connects to this one must use the same port number in order to
     * connect. */
    public static final int PORT = 12345;

    /** Left motor */
    private EV3LargeRegulatedMotor lMotor;

    /** Right motor */
    private EV3LargeRegulatedMotor rMotor;

    // add other variables here (motors, sensors, etc)


    /**
     * <p>
     * Main method; bootstraps and runs the robot.
     * </p>
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // construct the bot (you may need to add other parameters
        // to tell the robot about motors and sensors)
        RemoteBot rb = new RemoteBot();

        // listen for connections (this will call the connect() method
        // once a connection is established)
        rb.listen();
    }


    /**
     * <p>
     * Constructor.  Creates a new instance of the class and sets
     * it up so it's ready to go.  You should add code to this method
     * to perform any other necessary initialization (motors, sensors,
     * etc.).
     * </p>
     */
    public RemoteBot() {
        // Set the escape key to quit immediately as a failsafe
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });
        System.out.println("Press Escape to exit");

        // Change these ports to the ones you're using
        lMotor = new EV3LargeRegulatedMotor(MotorPort.A);
        rMotor = new EV3LargeRegulatedMotor(MotorPort.D);

        // Your code here: initialize any other motors, sensors, etc

    }


    /**
     * <p>
     * This method is called whenever a new request is received from the
     * network.  You must write code to inspect the message, take any
     * desired action, and return a response.
     * </p>
     *
     * @param r A message received from the network
     *
     * @return Any response generated after processing the request
     */
    public String request(String r) {
        // You'll likely want to check the "r" parameter, which
        // represents a message received from the network, and take
        // some kind of action based off of it.  Below is an example.
        // However, you'll need to change the String value to match
        // what your client is sending!

        // Note that you must use ".equals" and NOT "==" when checking
        // strings for equality!
        if ("red".equals(r)) {
            // make left LED red
            new EV3Led(EV3Led.LEFT).setPattern(2);
            // send back acknowledgement
            return "Lights on red";
        }
        else if ("off".equals(r)) {
            // turn left LED off
            new EV3Led(EV3Led.LEFT).setPattern(0);
            // send back acknowledgement
            return "Lights off";
        }
        else {
            // By default, this method simply logs a message that it didn't
            // process anything, and also returns that to the network client.

            // Show this message on the robot's screen
            System.out.println("Ignoring '" + r + "'");

            // Send this answer back over the network.  The client will
            // deal with it (perhaps displaying it)
            return "The server ignored '" + r + "'";
        }
    }



    /*
     *
     *
     * You do not need to edit below this comment, though you are
     * welcome to read and make changes to figure out how this works!
     *
     *
     *
     */


    /** LineSocket used to listen for messages */
    private LineSocket.Server server;


    /**
     * <p>
     * Listens for TCP/IP connections on the PORT specified above.
     * When a connection is made, control is handed off to the
     * connect() method above (which you must customize).
     * </p>
     *
     * <p>
     * When a connection closes, this method goes back to listening
     * for another connection.  If you only want to handle a single
     * connection before quitting, see the comments in the while loop
     * of the method.
     * </p>
     *
     * <p>
     * Otherwise, you do not need to make changes to this method.
     * </p>
     *
     */
    public void listen() {
        try {
            System.out.println("Starting server...");
            server = LineSocket.getServer((String r) -> request(r),
                                          PORT, null);
            System.out.println("Ready!\n");
            server.join(); // wait indefinitely for connections
        }
        catch (IOException ioe) {
            // report the error and quit
            System.out.println(ioe.toString());
            Delay.msDelay(30000);
        }
    }


}
