package org.suffieldacademy.lego.introcs;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * <p>
 * A <code>LineSocket</code> provides a simple line-oriented wrapper around
 * the <code>Socket</code> and <code>ServerSocket</code> classes.  It provides
 * two factory methods that return a concrete "client" or "server" class.
 * <b>For the impatient:</b> read the comments for the <code>getServer</code>
 * and <code>getClient</code> methods below.  You'll need to start one
 * Server and one Client and connect them together in order to communicate.
 * These subclasses operate with the following assumptions:
 * </p>
 *
 * <ul>
 * <li>
 * The <code>Client</code> class is responsible for initiating communication
 * at the protocol level; when its <code>request()</code> method is invoked
 * a message is sent to a remote <code>Server</code> and the client waits
 * for a response, which is then returned from the method.  The client does
 * not read from its socket at any other time, and assumes that it will only
 * receive data in response to it sending data first.
 * </li>
 *
 * <li>
 * The <code>Server</code> class is the opposite of the client.  It spawns
 * a thread and listens on a port for incoming connections.  <b>Only one
 * connection is processed at a time</b> (a client must close its connection
 * before another client can connect).  When a client connects, the server
 * waits for a line of text, which it then processes and sends back a
 * response.  A server assumes it will only respond to incoming requests;
 * it cannot send unsolicited messages to its socket.
 * </li>
 *
 * <li>
 * Because clients can only send, and servers can only receive, if you need
 * bi-directional communication (where either side can send data), you would
 * need to use two Client/Server pairs (one for each direction).
 * </li>
 * </ul>
 *
 * <p>
 * The <code>Client</code> and <code>Server</code> classes were designed to
 * work with each other.  They obey the request/response protocol an ordering
 * so you should not have deadlocks.  They can "talk" to other servers and
 * clients so long as they obey the general contract of sending/receiving
 * a single line at a time.
 * </p>
 *
 * <p>
 * To help with protocol enforcement, all messages sent between the Client
 * and Server must be <b>a single line of UTF-8 text</b>.  Any LF, CR, or
 * CRLF line breaks are stuffed into alternate representations before being
 * sent on the wire.  This makes the transmission slightly lossy (all line
 * endings will be stuffed and then unstuffed to a LF).
 * </p>
 *
 * @author Jason Healy
 *
 * @see LineSocket#getClient(Socket, Consumer)
 * @see LineSocket#getServer(Function, int, Consumer)
 *
 */
public abstract class LineSocket implements AutoCloseable {

    /** Handshake message to send/receive when connection is established */
    public static final String HANDSHAKE = "LineSocket 1.0.0 Init ";

    /** Escape sequence for newline characters */
    public static final String ESCAPE = "\u2028"; // Unicode LINE_SEPARATOR

    /** Keep track of whether this object is active */
    protected boolean alive;

    /** The active socket to communicate over */
    private Socket sock;

    /** The reader (input) for the active socket */
    protected BufferedReader in;

    /** The writer (output) for the active socket */
    protected BufferedWriter out;

    /** An optional callback to receive debug messages (may be null) */
    protected Consumer<String> debugger;


    /**
     * <p>
     * A base constructor intended for use by the concrete subclasses.
     * </p>
     *
     * @param sk The socket to make active (may be null)
     * @param d A callback to receive debugging events (see setDebugger())
     *
     * @throws IOException If the underlying socket throws an exception
     */
    protected LineSocket(Socket sk, Consumer<String> d) throws IOException {
        debugger = d;
        setSocket(sk);
        alive = true;
    }


    /**
     * <p>
     * Sets a callback for receiving debug event messages from the LineSocket.
     * Under Java 8+, a callback may be specified as a lambda expression;
     * for example, a LineSocket object ls could have its callback set by
     * using code like the following:
     * </p>
     *
     * <pre>
     * ls.setDebugger((String d) -> System.err.println(d));
     * </pre>
     *
     * <p>
     * That registers an anonymous function that takes the debug message
     * (String d) and prints it to STDERR.
     * </p>
     *
     * @param d The debug callback.  If null, no debug messages will be sent.
     */
    public void setDebugger(Consumer<String> d) {
        debugger = d;
    }


    /**
     * <p>
     * Sends the specified string to the debug callback.  If the callback is
     * null, no message is sent.
     * </p>
     *
     * @param s The debug message to send
     */
    protected void debug(String s) {
        if (null != debugger) {
            debugger.accept("DEBUG " + this.getClass().getName() + " " + s);
        }
    }


    /**
     * <p>
     * Sets the active socket for this LineSocket object.  For clients, this
     * method should be invoked once at initialization time.  For servers,
     * this method is called with a null value initially (as the server has
     * no connected clients), and then is called every time a new client
     * connects.
     * </p>
     *
     * <p>
     * This method also initializes the input/output streams for methods to
     * read from and write to.  If there is an existing socket, this method
     * attempts to close it and its streams prior to referencing the new
     * socket.
     * </p>
     *
     * @param sk The active socket (may be null)
     *
     * @throws IOException If any socket operation throws an exception
     */
    protected void setSocket(Socket sk) throws IOException {
        if (null != sock) {
            debug("setSocket() closing existing socket " + sock);
        }

        if (in != null) {
            try {
                in.close();
            }
            catch (IOException ioe) {}
            finally {
                in = null;
            }
        }

        if (out != null) {
            try {
                out.close();
            }
            catch (IOException ioe) {}
            finally {
                out = null;
            }
        }

        if (sock != null) {
            try {
                sock.close();
            }
            catch (IOException ioe) {}
            finally {
                sock = null;
            }
        }

        sock = sk;
        if (null != sock) {
            debug("setSocket() referencing new socket " + sock);

            in = new BufferedReader
                (new InputStreamReader(sock.getInputStream(), "UTF-8"));
            out = new BufferedWriter
                (new OutputStreamWriter(sock.getOutputStream(), "UTF-8"));
        }
        else {
            debug("setSocket() new socket is null");
        }
    }


    /**
     * <p>
     * Factory method to get a Client object.  Given a socket to transmit
     * over and an (optional) debugging callback, this method generates
     * a new Client that is ready to transmit.
     * </p>
     *
     * <p>
     * The following code is an example of initializing and using a Client:
     * </p>
     *
     * <pre>
     * try (LineSocket.Client c = LineSocket.getClient(
     *       new Socket("localhost", 12345),     // socket to connect to
     *       (String d) -> System.err.println(d) // debugging callback
     *     );) {
     *
     *     String response = c.request("My request");
     *     // ... other requests as desired ...
     * }
     * catch (IOException ioe) {
     *     ioe.printStackTrace(System.err);
     * }
     * </pre>
     *
     * <p>
     * As seen above, the Client establishes a connection, and then the user
     * may invoke its <code>request()</code> method as many times as
     * necessary.  The request() method sends the request and waits for a
     * response before returning (e.g., it is synchronous).  When the user
     * is done with the object, it should be closed.  The code above uses the
     * try-with-resources idiom to auto-close the Client; if not using this
     * you must invoke <code>close()</code> explicitly.
     * </p>
     *
     * <p>
     * For details on the debugging callback, please see setDebugger().  You
     * may specify a null callback; in this case no debugging messages will
     * be generated.
     * </p>
     *
     * @param sk The socket to communicate over
     * @param d A debugging callback (may be null; see setDebugger())
     *
     * @return A new Client object ready to issue requests
     *
     * @throws IOException If the underlying Socket throws any errors
     */
    public static Client getClient(Socket sk, Consumer<String> d)
        throws IOException {
        return new Client(sk, d);
    }


    /**
     * <p>
     * Convenience method to get a Client by specifying a host and port
     * instead of a socket.
     * </p>
     *
     * @see LineSocket#getClient(Socket, Consumer)
     */
    public static Client getClient(String host, int port, Consumer<String> d)
        throws IOException {
        return getClient(new Socket(host, port), d);
    }


    /**
     * <p>
     * Factory method to create a new Server object.  The server listens
     * on the specified port, and any messages received over the network
     * are sent to the given request handler for processing.  The results
     * after processing are sent back to the other endpoint of the socket.
     * </p>
     *
     * <p>
     * The following code is an example of initializing and using a Server:
     * </p>
     *
     * <pre>
     * try (LineSocket.Server s = LineSocket.getServer(
     *       (String req) -> "ACK: " + req,      // request handler callback
     *       1234,                               // port to listen on
     *       (String d) -> System.err.println(d) // debug callback
     *     );) {
     *
     *     s.join(); // waits until server errors out or close() is called
     * }
     * catch (IOException ioe) {
     *     ioe.printStackTrace(System.err);
     * }
     * </pre>
     *
     * <p>
     * As shown above, there are two callback parameters in addition to the
     * port to listen on.  The first callback is the request handler, and is
     * the code that will be called every time a message is received on the
     * socket.  This callback must accept a String parameter and return a
     * String result.  As shown above, the anonymous function provided takes
     * the input ("req") and generates a response by prepending some text to
     * it.
     * </p>
     *
     * <p>
     * For details on the debugging callback, please see setDebugger().  You
     * may specify a null callback; in this case no debugging messages will
     * be generated.
     * </p>
     *
     * <p>
     * The Server spawns a new thread to listen for incoming connections.
     * If your program does not wait on this thread, it may terminate before
     * any connections are received.  You may <code>join()</code> the Server,
     * which will cause your code to wait until the server exits (note that
     * this means your program may never end!).  If you have no other loop
     * that keeps your program running, you may want to do this.
     * </p>
     *
     * @param rh The request handler function
     * @param port The port to listen on
     * @param d A debugging callback (may be null; see setDebugger())

     * @return A new Server object, listening for connections
     *
     * @throws IOException If we cannot listen on the specified port
     */
    public static Server getServer(Function<String,String> rh, int port,
                                   Consumer<String> d) throws IOException {
        return new Server(rh, port, d);
    }


    /**
     * <p>
     * A utility method to remove line endings from strings so they can be
     * transmitted over a Socket as a single line.  Also replaces a null
     * string value with an empty string ("") to prevent exceptions.
     * </p>
     *
     * @param s The string to escape
     *
     * @return The string with all CR and LF characters escaped
     */
    protected static String escape(String s) {
        String t = s;
        if (null == t) {
            t = ""; // never transmit an actual null value
        }
        t = t.replaceAll("(\r\n|\r|\n)", ESCAPE);
        return t;
    }


    /**
     * <p>
     * A utility method to restore line endings that have been escaped from
     * a string using the escape() method.
     * </p>
     *
     * @param s The string to restore
     *
     * @return The string with LF characters re-inserted where they were
     * previously escaped.
     */
    protected static String unescape(String s) {
        String t = s;
        if (null == t) {
            t = "";
        }

        t = t.replace(ESCAPE, "\n");

        return t;
    }

    @Override
    public void close() throws IOException {
        debug("close() called; cleaning up");
        alive = false;
        sock.close();
    }


    /**
     * <p>
     * Concrete subclass of LineSocket that represents the "client" half
     * of a connection.  The Client class issues requests to a server and
     * waits for a response.  It does not receive unsolicited responses.
     * </p>
     *
     * <p>
     * You cannot instantiate this class directly; you should use the
     * LineSocket.getClient() method for that.
     * </p>
     *
     * @author Jason Healy
     *
     * @see LineSocket#getClient
     *
     */
    public static class Client extends LineSocket {

        /**
         * <p>
         * Constructor.  Creates a new client and associates it to the
         * given socket.
         * </p>
         *
         * @param sk The active socket to use
         * @param d A debugging callback (may be null)
         *
         * @throws IOException If the Socket throws any errors
         */
        private Client(Socket sk, Consumer<String> d) throws IOException {
            super(sk, d);
            debug("<init> looking for handshake from server...");
            String handshake = request(HANDSHAKE + this.getClass().getName());
            if (!handshake.startsWith(HANDSHAKE)) {
                alive = false;
                throw new IOException("Client received incorrect handshake: "
                                      + handshake);
            }
            debug("<init> handshake is correct");
        }


        /**
         * <p>
         * Issues a request to the connected socket and waits for and returns
         * the response that comes back.
         * </p>
         *
         * @param s The request message to send
         *
         * @return The response message received from the server
         *
         * @throws IOException If the underlying Socket throws an error
         */
        public String request(String s) throws IOException {
            if (!alive) {
                throw new IOException("Cannot write; Socket is closed");
            }

            // send the request
            debug("request() sending '" + s + "'");
            out.write(escape(s));
            out.newLine();
            out.flush();

            debug("request() send complete; waiting for response...");

            String r = in.readLine();

            debug("request() response was '" + r + "'");

            if (null == r) {
                throw new IOException("Null response from server; connection may be lost");
            }
            return unescape(r);
        }
    }


    /**
     * <p>
     * Concrete subclass of LineSocket that represents the "server" half
     * of a connection.  The Server class waits for inbound connections,
     * and when they connect, waits for request messages.  Those requests
     * are processed and the results sent back on the socket.  It does not
     * generate unsolicited messages.
     * </p>
     *
     * <p>
     * You cannot instantiate this class directly; you should use the
     * LineSocket.getServer() method for that.
     * </p>
     *
     * @author Jason Healy
     *
     * @see LineSocket#getServer
     */
    public static class Server extends LineSocket implements Runnable {

        /** The server socket we're listening with */
        private ServerSocket ss;

        /** A thread to wait on incoming requests */
        private Thread listenThread;

        /** The callback function to run when a request is received */
        private Function<String,String> receiver;


        /**
         *
         * <p>
         * Constructor.  Creates a server and starts it listening on the
         * specified port.
         * </p>
         *
         * @param rh The function to handle requests when they come in
         * @param port The port to listen on
         * @param d The callback to handle debug messages (may be null)
         *
         * @throws IOException If the underlying socket or server socket
         * throw an error
         */
        private Server(Function<String,String> rh,
                       int port, Consumer<String> d) throws IOException {

            super(null, d); // no socket yet
            try {
                debug("<init> binding to port " + port);
                ss = new ServerSocket(port);
            }
            catch (IOException ioe) {
                try {
                    close();
                }
                catch (IOException closingAnyway) {}
                throw ioe;
            }
            receiver = rh;
            listenThread = new Thread(this);
            listenThread.start();
        }


        @Override
        public void run() {
            debug("run() listening thread started");

            while (alive) {
                // listen for and accept a new connection
                try {
                    debug("run() waiting for incoming connection...");
                    setSocket(ss.accept());
                    debug("run() incoming connection established");
                }
                catch (IOException ioe) {
                    debug("run() server socket threw exception, disconnecting");
                    try {
                        close();
                    }
                    catch (IOException closingAnyway) {}
                }

                // read from that connection until it closes
                try {
                    // forward-declare variables
                    String line = null;
                    String p = null;

                    if (alive) {
                        debug("run() waiting for handshake from client...");
                        line = in.readLine();
                        p = unescape(line);
                        if (!p.startsWith(HANDSHAKE)) {
                            throw new IOException("Server received incorrect handshake: "
                                                  + p);
                        }
                        debug("run() got correct handshake, sending response");
                        out.write(HANDSHAKE + this.getClass().getName());
                        out.newLine();
                        out.flush();

                        debug("run() waiting for line from socket...");
                    }

                    while (alive && (line = in.readLine()) != null) {
                        debug("run() got '" + line + "'; processing...");
                        p = receiver.apply(unescape(line));
                        debug("run() processing returned '" + p + "'; returning to client...");
                        out.write(escape(p));
                        out.newLine();
                        out.flush();
                        debug("run() response flushed to client; waiting for another line...");
                    }
                    debug("run() client socket closed");
                }
                catch (IOException ioe) {
                    debug("run() socket threw '" + ioe + "'; closing");
                }
            }
            debug("run() server killed; shutting down");
        }


        /**
         * <p>
         * Waits until this Server object is done handling requests (either
         * an error on the server socket, or close() has been called).  Only
         * call this method if you're willing to wait for the server to be
         * done (which might be forever)!
         * </p>
         *
         * @see Thread#join()
         */
        public void join() {
            if (null != listenThread && listenThread.isAlive()) {
                try {
                    listenThread.join();
                } catch (InterruptedException ie) {
                    debug("join() was interruped " + ie);
                }
            }
        }

        @Override
        public void close() throws IOException {
            debug("LineSocket.Server close() called");
            super.close();
            if (ss != null) {
                ss.close();
            }
        }
    }


    /**
     * <p>
     * Simple self-test that can be run independently of other code.  Also
     * useful as an example for other programs.
     * </p>
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        try (LineSocket.Server s = LineSocket.getServer
             (
              (String l) -> "ACK: " + l, // response generator for each message
              1234, // port to listen on
              (String d) -> System.err.println(d) // code to run on each message
              );

             LineSocket.Client c = LineSocket.getClient
             (
              "localhost", // host to connect to
              1234, // port to connect to
              (String d) -> System.err.println(d) // code to run on each response received from the remote end
              );

             ) {

            for (int i = 0; i < 10; i++) {
                System.out.println("Sent " + i + ", response from server was"
                                   + " '" + c.request(""+i) + "'");
            }

            c.close();
        }
        catch (IOException ioe) {
            ioe.printStackTrace(System.err);
        }
        System.out.println("LineSocket test done!");
    }
}
