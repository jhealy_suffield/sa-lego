package org.suffieldacademy.lego.introcs;


import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.port.MotorPort;

import ev3dev.sensors.ev3.EV3ColorSensor;
import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * A program that uses a color sensor to follow a line on the ground.
 *
 * This is a skeleton class that includes a basic outline of a strategy
 * for line finding.  You are welcome to adapt it however you wish (you
 * don't need to follow the template exactly).  However, if you are at
 * all confused, I would recommend using my template until you have a
 * good understanding of what's going on before you make major changes.
 *
 * @author Your Name
 *
 */
public class LineFollower {

    /** The delimiter between "on" and "off" the line, as read by the
        color sensor.  The value here is a default, but should be updated
        by your code later based on measurements from the sensor.
    */
    private float threshold = 50;

    /** The left motor of the robot */
    private EV3LargeRegulatedMotor lMotor;

    /** The right motor of the robot */
    private EV3LargeRegulatedMotor rMotor;

    /** The sample provider from the color sensor */
    private SampleProvider csp;


    /**
     * Constructor.  Initializes any instance variables declared above
     * so the robot is ready to run.
     */
    public LineFollower() {
        // FIXME: you must make sure the ports below agree with how
        // you've constructed your robot
        lMotor = new EV3LargeRegulatedMotor(MotorPort.A);
        rMotor = new EV3LargeRegulatedMotor(MotorPort.D);
        EV3ColorSensor cs = new EV3ColorSensor(SensorPort.S1);
        csp = cs.getRedMode();
        // fetch a dummy value from the sensor so it is
        // initialized and ready to quickly take readings
        csp.fetchSample(new float[csp.sampleSize()], 0);
    } // end constructor


    /**
     * Starts the robot "executing" and performing the high-level tasks
     * that it needs to do.  This will be the first method run after
     * the contructor initializes everything.
     *
     * You do not need to edit this method (unless you want to); all
     * your code should go in the more specific methods below.
     */
    public void execute() {
        // you do not need to edit this method, but you are
        // welcome to make changes if you want to add features

        // only calibrate once, at the start
        findInitialLine();

        // so long as we can keep finding the line...
        while (findLine()) {
            // ...go forward until we lose it
            followLine();
        }

        System.out.println("\n\nfindLine() was false; all done");
        System.out.println("\nPress Escape to exit");
        Delay.msDelay(3600000); // quit automatically after an hour
    } // end execute()


    /**
     * Turns the robot (not the motors) by the specified number
     * of degrees, changing the relative heading of the robot.
     * A degrees value of 360 would cause the robot to turn in a
     * complete circle.
     *
     * @param degrees The change in heading for the robot.  Positive values
     *                turn the robot left; negative values turn it right.
     *
     * @param immediateReturn If true, the method returns as soon as the
     *                        motors have started.  If false, the method
     *                        waits until the rotation of the robot has
     *                        completed and the motors have stopped.
     */
    private void turn(int degrees, boolean immediateReturn) {
        // This method is used by other methods in the program to turn
        // the robot back and forth.  You should complete it early on
        // so you can calibrate and find the line.
    }


    /**
     * Turns the robot around in a 360-degree circle.  As the robot
     * turns, it should take readings from the color sensor and store
     * the brightest and darkest values that it sees during the turn.
     *
     * Once the turn is complete, the robot should set the "threshold"
     * variable to be roughly halfway between the minimum and maximum
     * readings (as a way to tell the difference between being on the
     * line and off).  If you wish, you may calculate this by using
     * value other than the average, but you MUST include a comment
     * explaining your methodology.
     *
     * When the circle is complete, the robot should turn back to
     * the point where the line was observed, then stop
     * its motors.
     *
     * Precondition: The robot is positioned so it is on the line,
     * or will rotate over the line once when it turns.
     *
     * Postconditions: The robot has rotated so that the color sensor
     * is at the location where it was over the line, and the threshold
     * value has been calibrated.
     */
    private void findInitialLine() {
        EV3Key enter = new EV3Key(EV3Key.BUTTON_ENTER);
        System.out.println("\nPress escape to quit anytime");
        System.out.println("\n\nPlace the robot on or near the line start.");
        System.out.println("\nThen, press enter to calibrate.");
        enter.waitForPressAndRelease();
        System.out.println("\033[2J");

        // These are some variables to keep track of the smallest
        // and largest readings you get while turning in a circle.
        float min = Float.POSITIVE_INFINITY;
        float max = Float.NEGATIVE_INFINITY;

        lMotor.resetTachoCount();
        rMotor.resetTachoCount();

        // These variables are to help you remember "where" the motor
        // was when you think you saw the line.  Every time you take
        // a new sample, if it's "better" than any other sample you've
        // seen you should update these variables with the tacho count
        // of each motor.
        int lBest = lMotor.getTachoCount();
        int rBest = rMotor.getTachoCount();

        // You will need to create an array to hold a sample from the
        // light sensor here:

        // Turn using turn(), and take samples while its turning
        //
        // As you turn, if you discover a new min or max value, update
        // the variables.

        // When you've finished turning, turn back to the location
        // where you saw the best reading.

        // Finally, calculate a new threshold value based on the min
        // and max values you observed.

    }


    /**
     * Moves the robot forward, constantly taking readings from
     * the color sensor.  When the color sensor shows that the
     * robot is no longer on the line, stop the motors.
     *
     * Precondition: The robot is on the line.
     *
     * Postcondition: The robot has moved forward until it no
     * longer detected the line.  The motors are stopped.
     */
    private void followLine() {
        // You will need to complete this method to have the robot
        // move forward and attempt to follow the line

        // turn the motors on

        // loop and take samples until they show you're not on the line

        // stop the motors
    }


    /**
     * Searches for the line by turning the robot back in forth in
     * progressively larger sweeps until it finds the line or gives
     * up (can't find).  You may want to give up after turning in
     * a complete circle and still not being able to find the line.
     *
     * Precondition: The motors are stopped.
     *
     * Postcondition: The robot is once again on the line (returns true)
     *                or has given up looking (returns false).
     *
     * @return boolean True, if the robot found the line again.
     *                 False if the robot could not find the line.
     */
    private boolean findLine() {

        return false; // REMOVE THIS LINE AND REPLACE WITH YOUR CODE


        // You should create a sample array to fetch data.

        // Then, have a large loop that continues to execute
        // progressively larger turns.

        // While you're making each turn, loop and take samples
        // from the sensor and see if you're back "on" the line.
        // If you are, stop the motors and return true.

        // If you execute all the turns you want to try, return
        // false by default so the program gives up.
    }


    /**
     * Initial entry point into the program.  You shouldn't need to edit this.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Set the escape key to quit immediately as a failsafe
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        // Create a new LineFollower object...
        LineFollower lf = new LineFollower();
        // ... and set it running
        lf.execute();

    } // end main()


} // end class LineFollower
