package org.suffieldacademy.lego.introcs;


import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.port.MotorPort;

import ev3dev.sensors.ev3.EV3TouchSensor;
import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * A program to control a robot so it goes forward until it hits something,
 * then turns 90 degrees, and repeats this process 4 times until it stops
 * facing the same direction as it started.
 *
 * You must complete the comments to the methods below, especially the
 * "precondition" and "postcondition" for each one.
 *
 * Those conditions help you (and someone reading your code) to understand
 * how the different methods work together.  The preconditions should state
 * what the method assumes to be true about the robot just before its called.
 * The postconditions are what will be true just as the method finishes.
 *
 * In this case, the conditions should mainly deal with the state of the
 * robot.  Are its motors running?  Where is the robot?  Which way is it
 * facing?  Because the program uses multiple methods, one right after
 * another, you (the programmer) have to decide where certain actions
 * take place.  The pre and post conditions document those decisions so
 * someone else can understand that without having to read all your code.
 *
 * @author Your Name Here
 */
public class FourWallBot {

    // YOUR CODE HERE

    // Because this program uses multiple methods, you must declare
    // any variables shared between methods here (outside of any method,
    // but inside the class).  Otherwise, the variables in one method
    // can't be seen in other methods.

    // You declare methods just as you did in Processing, but add
    // "private" in front of them, like the example below.  Make sure
    // to comment each of your variables!

    // Here's an example, declaring a motor variable:
    /** The left motor */
    private EV3LargeRegulatedMotor lMotor;

    // You'll notice there's no equals sign on the line above (no
    // assignment).  Declare any other variables you need here, and
    // then initialize them in your constructor below.  You'll likely
    // need another motor and a touch sensor...



    /**
     * Constructor.  Initializes any instance variables declared above
     * so the robot is ready to run.
     */
    public FourWallBot() {
        // Your code here

        // You should initialize variables for motors and sensors,
        // and any others you deem necessary.

        // You should take one "dummy" reading from your sample
        // provider, which will cause the sensor to initialize.
        // If you don't do it here, you may notice a lag of a few
        // seconds the first time you take a reading.

        // You should NOT move the robot or do any "work"; this
        // constructor is just to get everything set up.  The
        // execute() method below is responsible for "running"
        // the robot to perform its task.
    } // end constructor


    /**
     * Starts the robot "executing" and performing the high-level tasks
     * that it needs to do.  This will be the first method run after
     * the contructor initializes everything.
     *
     * You do not need to edit this method (unless you want to); all
     * your code should go in the more specific methods below.
     */
    public void execute() {

        // Print a message and wait for the user to push the
        // enter button
        EV3Key enter = new EV3Key(EV3Key.BUTTON_ENTER);
        System.out.println("\n\nPress enter to start");
        System.out.println("\nPress escape to quit");
        enter.waitForPressAndRelease();

        // We want the robot to execute the same sequence of moves
        // 4 times, so we loop and run "helper" methods that each
        // handle a well-defined part of the movement
        for (int i = 0; i < 4; i += 1) {
            goForward();
            waitForHit();
            backUp();
            turnRight();
        }

        System.out.println("\nDone!\nPress escape or wait 15 seconds for quit.");
        Delay.msDelay(15000);
    } // end execute()


    /**
     * Starts the motors turning so the robot goes forward.
     *
     * Precondition:
     *
     * Postcondition:
     *
     */
    private void goForward() {
        // your code here
    } // end goForward()


    /**
     * Method "waits" (does not finish) until the robot has hit something.
     *
     * Precondition:
     *
     * Postcondition:
     *
     */
    private void waitForHit() {
        // your code here

        // hint: you'll want to sample the touch sensor and use a loop
        // to keep you in this method until you get a sample with a
        // specific value
    } // end waitForHit()


    /**
     * Backs the robot up a short distance after a hit.
     *
     * Precondition:
     *
     * Postcondition:
     *
     */
    private void backUp() {
        // your code here
    } // end backUp()


    /**
     * Turns the robot so it is facing 90 degrees to the right from
     * its initial position.
     *
     * Precondition:
     *
     * Postcondition:
     *
     */
    private void turnRight() {
        // your code here

        // hint: to turn (instead of just going forward or backward),
        // the motors have to move in opposite directions

        // you'll need to experiement with different values to get
        // the robot to turn exactly 90 degrees
    } // end turnRight()




    /**
     * Initial entry point into the program.  You shouldn't need to edit this.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Set the escape key to quit immediately as a failsafe
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        // Create a new FourWallBot object...
        FourWallBot fwb = new FourWallBot();
        // ... and set it running
        fwb.execute();

    } // end main()


} // end class
