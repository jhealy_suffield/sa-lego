package org.suffieldacademy.lego.introcs;


import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.port.MotorPort;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * This program runs a two-motor robot through a specified number of
 * rotations.  You may edit the constants at the top of the class that
 * define where the motors are connected and which direction they should
 * run.  Otherwise, you should make no changes to the program.
 *
 * The object is to build a robot that travels the SMALLEST distance
 * possible, without crippling the robot (disconnecting the motors,
 * using non-round wheels, etc).
 *
 * @author Jason Healy
 */
public class SlowBot {

        // You may edit these variables to match the ports your motors are
        // connected to, and to change their direction (if necessary)

        /** The port that the left motor is connected to (A, B, C, or D) */
        private static final Port LEFT_MOTOR_PORT = MotorPort.A;

        /** If your left motor needs to turn in the opposite direction,
            make this true */
        private static final boolean LEFT_MOTOR_REVERSED = false;

        /** The port that the right motor is connected to (A, B, C, or D) */
        private static final Port RIGHT_MOTOR_PORT = MotorPort.D;

        /** If your right motor needs to turn in the opposite direction,
            make this true */
        private static final boolean RIGHT_MOTOR_REVERSED = false;



        /////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////
        /////          DO NOT CHANGE ANYTHING BELOW THIS POINT          /////
        /////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////

        /** Number of complete revolutions for each motor */
        public static final int REVOLUTIONS = 30;

        /** Left motor target rotation (taking direction into account) */
        private static final int LEFT_ANGLE_LIMIT = REVOLUTIONS * 360
            * (LEFT_MOTOR_REVERSED ? -1 : 1);

        /** Right motor target rotation (taking direction into account) */
        private static final int RIGHT_ANGLE_LIMIT = REVOLUTIONS * 360
            * (RIGHT_MOTOR_REVERSED ? -1 : 1);

        /** Left motor control object */
        private static EV3LargeRegulatedMotor lMotor;

        /** Right motor control object */
        private static EV3LargeRegulatedMotor rMotor;


        /**
         * Initial entry point into the program.
         *
         * @param args
         */
        public static void main(String[] args) {
            System.out.println("Initializing motors...");

            // Set the escape key to quit immediately
            new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

            // Create a key that we can wait on
            EV3Key enter = new EV3Key(EV3Key.BUTTON_ENTER);

            // Set up the motors
            lMotor = new EV3LargeRegulatedMotor(LEFT_MOTOR_PORT);
            rMotor = new EV3LargeRegulatedMotor(RIGHT_MOTOR_PORT);

            while (true) {
                // Note that this issues a "reset" to the motor, which cancels
                // all speed, stop, and limit angle settings (so issue those
                // commands again below)
                lMotor.resetTachoCount();
                rMotor.resetTachoCount();

                // rotate and rotateTo use the last-applied stopping mechanism
                // (coast, hold, brake, etc).  We want to "hold" so we explicitly
                // do that here to prime the mode for the subsequent rotate
                lMotor.hold();
                rMotor.hold();

                // Set to a medium-fast speed (not maximum) to start
                int speed = 600;

                System.out.println("\n\nPress enter to start rotating");
                System.out.println("\nPress escape to quit");
                enter.waitForPressAndRelease();

                // use an iterative approach with slower and slower speeds to
                // home in on the final angle value
                while (speed > 50) {

                    lMotor.setSpeed(speed);
                    rMotor.setSpeed(speed);

                    // only move if the angle isn't correct
                    if (lMotor.getTachoCount() != LEFT_ANGLE_LIMIT) {
                        lMotor.rotateTo(LEFT_ANGLE_LIMIT, true);
                    }
                    if (rMotor.getTachoCount() != RIGHT_ANGLE_LIMIT) {
                        rMotor.rotateTo(RIGHT_ANGLE_LIMIT, true);
                    }

                    // provide updates on the motor position
                    while (lMotor.isMoving() || rMotor.isMoving()) {
                        printTachoCounts();
                        Delay.msDelay(100);
                    }

                    // reduce the speed by half, ensuring termination in
                    // log2(speed)
                    speed = speed / 2;
                }

                // print final motor position
                printTachoCounts();
            }
        }


        /**
         * Clear the screen and print the current motor positions.
         */
        private static void printTachoCounts() {
            System.out.printf("\033[2J\nMotor position:\n  L:%6d / %-6d\n  R:%6d / %-6d",
                              Math.abs(lMotor.getTachoCount()),
                              Math.abs(LEFT_ANGLE_LIMIT),
                              Math.abs(rMotor.getTachoCount()),
                              Math.abs(RIGHT_ANGLE_LIMIT));
        }


} // end class
