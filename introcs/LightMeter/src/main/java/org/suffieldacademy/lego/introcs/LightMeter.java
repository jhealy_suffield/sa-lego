package org.suffieldacademy.lego.introcs;


import ev3dev.sensors.ev3.EV3ColorSensor;

import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * <p>
 * Complete the following class, which creates a device that measures
 * ambient light levels near the brick.  In addition to writing code,
 * you must edit the comments below and answer the following questions:
 * </p>
 *
 * With the sensor in AMBIENT mode:
 *
 * 1) What is the minimum sample value you can get from the sensor?
 *
 * 2) What did you point it at to get that value?  How far away was
 * the sensor from the target?
 *
 * 3) What is the maximum sample value you can get from the sensor?
 *
 * 4) What did you point it at to get that value?  How far away was
 * the sensor from the target?
 *
 * 5) What do you need to point the sensor at in order to get a reading
 *    halfway between the minimum and maximum values?
 *
 *
 * With the sensor in RED mode (change your code and re-run with the new mode):
 *
 * 6) Hold the sensor close (1cm) to a surface.  What surface can you
 *    point the sensor at that gives the minimum sample value?
 *
 * 7) What surface can you point the sensor at that gives the maximum
 *    sample value?
 *
 * 8) What surface gives a reading roughly halfway between the minimum
 *    and maximum values?
 *
 *
 * @author Your Name
 */
public class LightMeter {

    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Set the escape key to quit immediately
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        // Your code here.  You will need to declare and initialize
        // an EV3ColorSensor, and then access its "Ambient" or "Red"
        // mode (as appropriate, see the questions at the top of this file)
        //
        // If you're not sure how to access the modes, please refer to the
        // docuementation for the EV3ColorSensor class:
        //
        //  https://ev3dev-lang-java.github.io/docs/api/latest/ev3dev-lang-java/ev3dev/sensors/ev3/EV3ColorSensor.html
        //

        // Once you have that, you'll need to fetch samples in a loop;
        // you can use "while (true)" for this, as the Escape key will
        // quit the program when pressed even if the loop is running.

        // Have the loop print out each sample on the screen.  You may
        // want to include a delay so the lines on the screen stay
        // long enough for you to read them.

        // Using your data, answer the questions in the comments at the
        // top of this file.

        // One final hint: you can clear the screen by printing a special
        // character sequence out.  It looks like gibberish but it means
        // something to the computer:
        //
        // System.out.println("\033[2J");
        //
        // You can use that to show a single sample on the screen at a time
        // (if you want; otherwise if you print the lines will just scroll
        // off the top of the screen as new ones appear).

    } // end main()


} // end class
