package org.suffieldacademy.lego.introcs;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.port.MotorPort;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;

import ev3dev.actuators.ev3.EV3Led;
import ev3dev.actuators.Sound;


/**
 * <p>
 * A skeleton class for a robot that can communicate with another
 * computer or robot using an IP connection.  You will need to complete
 * the acceptLoop() method to set up the network connection.
 * </p>
 *
 * @author Your Name
 *
 */
public class NetworkIntro {

    /**
     * <p>
     * Waits for network connections forever.  Each time one is
     * accepted, it reads from the socket and prints out what
     * is received (and optionally performs other actions).
     * </p>
     */
    public void acceptLoop() {
        // YOUR CODE HERE
        // Erase this comment and replace it with code from the
        // lab instructions.
    }


    /**
     * <p>
     * Constructor.  Creates a new instance of the class and sets
     * it up so it's ready to go.  For this lab, you don't need to
     * make any changes unless you add optional features to the
     * final code (such as controlling motors).
     * </p>
     */
    public NetworkIntro() {
        // Set the escape key to quit immediately as a failsafe
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });
        System.out.println("Press Escape to exit\n");
    }


    /**
     * <p>
     * Main method; bootstraps and runs the robot.  You do not need
     * to make changes to this method.
     * </p>
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // construct the bot (you may need to add other parameters
        // to tell the robot about motors and sensors)
        NetworkIntro rb = new NetworkIntro();

        // listen for connections (this will call the connect() method
        // once a connection is established)
        rb.acceptLoop();

        System.out.println("\nacceptLoop() exited");
        Delay.msDelay(30000);
    }


}
