package org.suffieldacademy.lego.example;


import lejos.utility.Delay;


/**
 * <p>
 * This is a simple test class that runs the most minimal code possible
 * on the LEGO brick.
 * </p>
 *
 * @author Jason Healy
 */
public class HelloWorld {

    /**
     * Initial entry point into the program.
     *
     * @param args
     */
    public static void main(String[] args) {
        // Just print a message to the console.  This should show up
        // on your brick's screen
        System.out.println("\n\n*\n*\n*\n*  Hello World!\n*\n*\n*");

        // pause execution for 10 seconds so you can see the message
        Delay.msDelay(10000);
    }
}
