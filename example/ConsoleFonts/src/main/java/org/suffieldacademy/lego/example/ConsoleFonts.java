package org.suffieldacademy.lego.example;


import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import ev3dev.utils.Shell;

import lejos.utility.Delay;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.PathMatcher;

import java.io.IOException;

import java.util.stream.Stream;
import java.util.Iterator;


/**
 * <p>
 * Program to run through installed console fonts and display samples.
 * As written, assumes you want to look only at English-language fonts;
 * there are fields that you may edit to examine other families of fonts.
 * </p>
 *
 * <p>
 * Once you have the name of a font that you want to use, you can put a
 * line like this in your source code to use the new font:
 * </p>
 *
 * <pre>
 * ev3dev.utils.Shell.execute(new String[] {"setfont", "NAME OF FONT"});
 * </pre>
 *
 * <p>
 * The author has a few font suggestions for quick reference:
 * <b>Lat15-Terminus12x6</b> for the smallest-but-still-legible, and
 * <b>Lat15-TerminusBold14</b> for a medium and high legibility default.
 * Other thoughts below:
 * </p>
 *
 * <ul>
 *
 * <li>Lat15-TomThumb4x6: Default font, very small, can fit a lot on
 * screen but can be difficult to read.</li>
 *
 * <li>Lat15-Terminus12x6: small but not illegible.  A good choice if
 * the default is too small but you want to fit as much text as possible.</li>
 *
 * <li>Lat15-Fixed13: Medium-sized legible font, light weight and not
 * too tall.  The 14,15,16,18 variants are not much bigger and are
 * quite similar.</li>
 *
 * <li>Lat15-TerminusBold14:  Vertically similar to the other 14 fonts,
 * but heavier and easier to read (better contrast) at the expense of
 * not fitting as much on a line.  In larger (20+) sizes they look similar
 * to the non-bold variants.</li>
 *
 * <li>Lat15-Terminus20x10: Big but not huge; light weight.</li>
 *
 * <li>Lat15-TerminusBold22x11: Largest practical font size (can easily
 * read at arm's length).</li>
 *
 * <li>Lat15-TerminusBold32x16: Huge; can only fit 4 lines with 11
 * chars each.</li>
 *
 * <li>Lat15-VGA*: Bold, good contrast, a little wide.  More
 * "bitmapped" appearance at larger sizes than Terminus.</li>
 *
 * </ul>
 *
 * @author Jason Healy
 */
public class ConsoleFonts {

    /**
     * Shell glob pattern for matching fonts to trial.  We are only
     * looking for English-character fonts, so we choose the most restrictive
     * set to save memory.  See man (5) console-setup "CODESETS" for more
     * information.  We're using the "Lat15" set which covers English
     * though the larger Uni? sets would also work but be larger).
     */
    public static final String FONT_GLOB = "glob:**/Lat15-*.psf.gz";

    /** Font to restore when program is done.  See /etc/default/console. */
    public static final String DEFAULT_FONT = "Lat15-TomThumb4x6";

    /** Flag to allow early quit */
    private static boolean running = true;

    /** Flag to pause between fonts */
    private static boolean paused = true;


    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Set up quit listener
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    running = false;
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        // set up pause listener
        new EV3Key(EV3Key.BUTTON_ENTER).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    paused = false;
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        System.out.println("Press enter to cycle through fonts");
        waitForButtons();

        Path fontPath = Paths.get("/", "usr", "share", "consolefonts");
        PathMatcher filter = fontPath.getFileSystem()
            .getPathMatcher(FONT_GLOB);
        try (Stream<Path> stream = Files.list(fontPath)) {
            Iterator<Path> itr = stream.filter(filter::matches).sorted().iterator();
            while (running && itr.hasNext()) {
                String fullfont = itr.next().getFileName().toString();

                // try to remove the font suffix
                int dotidx = fullfont.indexOf(".psf.gz");
                String font = fullfont;
                if (dotidx > -1) {
                    font = fullfont.substring(0, dotidx);
                }

                // clear the screen
                System.out.println("\033[2J");

                // set the font
                System.out.println(Shell.execute(new String[] {"setfont", font}));
                // display the font name in its own font
                System.out.println(font);
                waitForButtons();
            }
        }
        catch (IOException ioe) {
            System.out.println("Unable to list files: " + ioe);
        }

        // clean up and put the font back to the system default
        System.out.println(Shell.execute(new String[] {"setfont",
                                                       DEFAULT_FONT}));

        if (running) {
            System.out.println("\033[2J");
            System.out.println("All done; press enter to quit.");
            waitForButtons();
        }
    }


    /**
     * Suspend execution until either the Enter or Escape button is pressed.
     */
    private static void waitForButtons() {
        while (paused && running) {
            Delay.msDelay(50);
        }
        // restore the enter latch now that we're done waiting
        // (running is never set back to true so we can quit immediately)
        paused = true;
    }
}
