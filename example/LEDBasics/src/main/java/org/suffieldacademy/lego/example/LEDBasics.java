package org.suffieldacademy.lego.example;

import ev3dev.actuators.ev3.EV3Led;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * <p>
 * This program demonstrates how to interact with the LED lights
 * build in to the EV3.  There are two "sides" (LEFT and RIGHT),
 * and each side has multiple colors available (red, green, yellow).
 * </p>
 *
 * <p>
 * https://ev3dev-lang-java.github.io/docs/api/latest/ev3dev-lang-java/ev3dev/actuators/ev3/EV3Led.html
 * </p>
 *
 * <p>
 * https://www.ev3dev.org/docs/tutorials/using-ev3-leds/
 * </p>
 *
 * @author Jason Healy
 */
public class LEDBasics {

    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Button for user interaction
        EV3Key enter = new EV3Key(EV3Key.BUTTON_ENTER);

        // To control the LEDs, you must create an EV3Led object
        // and tell it which LED side you want to operate:
        EV3Led l = new EV3Led(EV3Led.LEFT);
        EV3Led r = new EV3Led(EV3Led.RIGHT);

        // According to the JavaDocs, there are 10 patterns available
        // for the LEDs.  We name them here.  HOWEVER, see below...
        String[] desc =
            { "Off",
              "Green Solid",
              "Red Solid",
              "Yellow Solid",
              "Green Blink",
              "Red Blink",
              "Yellow Blink",
              "Green Fast",
              "Red Fast",
              "Yellow Fast"
            };

        // If you read the actual source code, only modes 0-3
        // are actually implemented:
        //
        // https://github.com/ev3dev-lang-java/ev3dev-lang-java/blob/master/src/main/java/ev3dev/actuators/ev3/EV3Led.java
        //
        // So, we'll just loop through 0-3:

        // loop over each supported pattern type...
        for (int i = 0; i < 4; i = i + 1) {
            // ... set the pattern on the LEDs ...
            l.setPattern(i);
            r.setPattern(i);

            // ... and display the pattern number and description
            System.out.println("\033[2JLEDBasics\n" + i + ": "
                               + desc[i] + "\n");
            System.out.println("Press enter for next.");
            enter.waitForPressAndRelease();
        }

        System.out.println("All done!");
        Delay.msDelay(2000);
    }


} // end class
