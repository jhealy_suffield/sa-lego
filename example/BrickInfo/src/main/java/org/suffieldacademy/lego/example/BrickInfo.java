package org.suffieldacademy.lego.example;

import ev3dev.hardware.EV3DevPlatforms;
import ev3dev.hardware.EV3DevDistros;
import ev3dev.sensors.Battery;
import ev3dev.sensors.Button;


/**
 * <p>
 * A program that gets some basic information from the hardware and
 * displays it on the screen.  Also includes a simple change to the
 * console font and a basic button-wait.
 * </p>
 *
 * @author Jason Healy
 */
public class BrickInfo {

    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        System.out.printf("Platform: %s\n",
                          EV3DevPlatforms.getInstance().getPlatform());

        System.out.printf("Distribution: %s\n",
                          EV3DevDistros.getInstance().getDistro());

        System.out.printf("Battery Voltage: %dmV\n",
                          Battery.getInstance().getVoltageMilliVolt());

        // Wait for a button to be pressed so you can read the screen
        // before the program quits
        System.out.println("\nPress any button to quit.");
        Button.waitForAnyPress();
    }
}
