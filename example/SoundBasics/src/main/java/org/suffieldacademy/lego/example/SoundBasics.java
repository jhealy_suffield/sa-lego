package org.suffieldacademy.lego.example;

import ev3dev.actuators.Sound;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * <p>
 * </p>
 *
 * <p>
 * https://ev3dev-lang-java.github.io/docs/api/latest/ev3dev-lang-java/ev3dev/actuators/Sound.html
 * </p>
 *
 * @author Jason Healy
 */
public class SoundBasics {

    // To control the sound, we must get an instance of the Sound
    // class.  However, you don't say "new", but rather ask for
    // an instance (this is to prevent multiple Sound objects from
    // existing at the same time).  Because we're going to use it
    // from multiple methods, we declare it as an instance variable.
    private static Sound s = Sound.getInstance();

    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Set the escape key to quit immediately (not related directly
        // to sound)
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });


        // Create a key to wait on
        EV3Key enter = new EV3Key(EV3Key.BUTTON_ENTER);


        // Add volume up/down handlers so the volume can be changed
        // at any time
        new EV3Key(EV3Key.BUTTON_UP).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    int vol = s.getVolume();
                    vol += 10;
                    if (vol <= 100) {
                        s.setVolume(vol);
                    }
                    s.beep();
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        new EV3Key(EV3Key.BUTTON_DOWN).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    int vol = s.getVolume();
                    vol -= 10;
                    if (vol >= 0) {
                        s.setVolume(vol);
                    }
                    s.beep();
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        System.out.println("\nCurrent volume: " + s.getVolume() + "/100");
        System.out.println("Press UP/DOWN to change");
        System.out.println("\nPress enter to\ncontinue");
        enter.waitForPressAndRelease();

        // You can play one of two built-in sounds by simply running
        // the appropriate methods:
        System.out.println("\033[2JSoundBasics: Beep");
        s.beep();
        System.out.println("\nPress enter to\ncontinue");
        enter.waitForPressAndRelease();

        System.out.println("\033[2JSoundBasics: Beeps");
        s.twoBeeps();
        System.out.println("\nPress enter to\ncontinue");
        enter.waitForPressAndRelease();


        System.out.println("\033[2JSoundBasics: Tone");
        // You can also play pitched tones by specifying a frequency
        // and duration (in milliseconds).
        s.playTone(1000, 2000); // 1000 Hz for 2 seconds

        System.out.println("\nPress enter to\ncontinue");
        enter.waitForPressAndRelease();

        // If you look up the frequency values for different notes,
        // you can play simple songs!
        System.out.println("\033[2JSoundBasics: Song\n");
        System.out.println("\nPress enter and I\nwill sing you a song.");
        enter.waitForPressAndRelease();

        // pitches
        final int D = 294;
        final int E = 330;
        final int G = 392;
        final int Fs = 370;

        // durations
        final int N = 400;

        System.out.print("Ba");
        s.playTone(D, N*3/2);

        System.out.print("by");
        s.playTone(E, N*3/2);

        for (int i = 0; i < 3; i += 1) {
            System.out.print(" shark\n");
            s.playTone(G, N/2);

            System.out.print("doo");
            s.playTone(G, N/2);

            System.out.print(" doo");
            s.playTone(G, N/2);

            System.out.print(" doo");
            s.playTone(G, N/4);

            System.out.print(" doo");
            s.playTone(G, N/2);

            System.out.print(" doo");
            s.playTone(G, N/4);

            System.out.print(" doo\n\n");
            s.playTone(G, N/2);

            if (i < 2) {
                System.out.print("Ba");
                s.playTone(D, N/2);

                System.out.print("by");
                s.playTone(E, N/2);
            }
        }

        System.out.print("Ba");
        s.playTone(G, N/2);

        System.out.print("by");
        s.playTone(G, N/2);

        System.out.print(" shark\n");
        s.playTone(Fs, N);

        System.out.println("\nPress enter to\ncontinue");
        enter.waitForPressAndRelease();

        // You can play a sound file through the speaker.  Be aware that
        // the sound quality isn't all that good!  You should generally
        // only play short sound clips (not whole songs) as the memory
        // of the brick is limited.
        //
        // Save the sound file inside the folder "src/main/resources"
        // in your project (create "resources" if it doesn't exist).
        // It will be copied to the brick along with your code, and
        // the program can find it by its name.

        // This sound file licensed CC-0:
        // https://freesound.org/people/kirbydx/sounds/175409/
        System.out.println("\033[2JSoundBasics: Sample\n");
        s.playSample("trombone-16bit-44khz.wav");
        System.out.println("\nPress enter to\ncontinue");
        enter.waitForPressAndRelease();
    }


} // end class
