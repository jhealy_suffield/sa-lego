package org.suffieldacademy.lego.example;


import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.port.MotorPort;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * <p>
 * This program demonstrates the setup, initialization, and use of
 * the EV3 "large" motor.  The comments contain some additional hints
 * about use of the motors and caveats.  Note that the ev3dev environment
 * is under constant development, so some comments may be out of date.
 * This file was last updated on 2020-02-15.
 * </p>
 *
 * @author Jason Healy
 */
public class MotorBasics {

    /** Motor control object */
    private static EV3LargeRegulatedMotor m;

    /** Create a key that we can wait on */
    private static EV3Key enter;


    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Set the escape key to quit immediately (not related directly
        // to motors)
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    m.stop(); // try to stop the motor so it doesn't
                              // keep running after quit
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        // Create a key that we can wait on
        enter = new EV3Key(EV3Key.BUTTON_ENTER);


        System.out.println("Initializing motor...");

        // You must declare and construct a motor using a named constant
        // for the port where the motor is connected.  In this example,
        // it's in port A but you can change to A-D as desired.
        m = new EV3LargeRegulatedMotor(MotorPort.A);

        taskPause();
        basicForward();

        taskPause();
        rotateTwice();

        taskPause();
        rotateTo();

        taskPause();
        rotateWithStops();

        taskPause();
        rotateSimultaneous();

        taskPause();
        liveSpeedChange();

        System.out.println("\033[2J\nAll tasks complete\n\nHit enter to quit");
        enter.waitForPressAndRelease();
    }


    /**
     * A helper function that prints a message and waits for the user to
     * press the Enter key on the brick before continuing.  Once the user
     * presses the button, the motor is reset and the screen is cleared.
     * Use this method between other tasks to suspend execution and allow
     * the user to read what's on the screen before continuing.
     */
    public static void taskPause() {
        System.out.println("Hit enter for next task");
        enter.waitForPressAndRelease();

        // The tacho count keeps track of how far the motor has rotated.
        // In this version (2.6.2) issuing the reset also clears all
        // other attributes on the motor (such as speed, stop type, and
        // limit angle settings).  Be aware that if you use this command
        // you will need to re-issue any property settings you had before.
        m.resetTachoCount();

        // clear screen
        System.out.print("\033[2J");
    }


    /**
     * A basic example that runs the motor for a set period of time.
     */
    public static void basicForward() {
        System.out.println("basicForward()\n");
        System.out.println("Running motor for 5 seconds at 360deg/sec");
        m.setSpeed(360);     // set the speed (must do before running)
        m.forward();         // start motor running "forever"
        Delay.msDelay(5000); // wait 5 seconds
        m.stop();            // stop the motor

        // We can find out how many degrees the motor turned
        System.out.printf("\nMotor ran for %d degrees\n",
                          m.getTachoCount());
    }


    /**
     * A basic example showing how you can rotate by a specific amount.
     */
    private static void rotateTwice() {
        System.out.println("rotateTwice()\n");
        System.out.println("Rotating motor by 720 degrees");
        m.setSpeed(180);
        m.rotate(720); // this line won't "finish" until the motor stops
                       // by itself by turning the correct amount
        System.out.println("\nDone.  Now press enter to rotate backwards by 720");
        enter.waitForPressAndRelease();
        m.rotate(-720); // a negative amount turns the other way
    }


    /**
     * A basic example showing how you can rotate to a specific position.
     */
    private static void rotateTo() {
        m.flt(); // set the motor not to resist motion

        while (enter.isUp()) {
            System.out.printf("\033[2J\nrotateTo()\nTurn the motor with your hand and watch readings.  Press enter to rewind to zero.\n\nAngle: %d",
                              m.getTachoCount());
            Delay.msDelay(50);
        }

        m.setSpeed(360);
        m.rotateTo(0); // rotate until the tachometer reads zero
        System.out.printf("\n\nFinal angle is: %d\n", m.getTachoCount());
    }


    /**
     * A basic example showing how you can rotate to a specific position.
     */
    private static void rotateWithStops() {
        // ev3dev differs from lejOS in that the "stop" action after a
        // rotate() or rotateTo() depends on the last stop action that
        // was explicitly requested.  So you may need to "seed" the stop
        // action by calling a specific action prior to initiating the
        // rotation method.
        //
        // Note well: if you call resetTachoCount(), that resets the
        // stop command assigned to the motor (it reverts to "coast").
        // If you issue resetTachoCount() directly after a rotation,
        // it will seem like you've set it to coast, as any "hold"
        // or "brake" will be disabled the instant the motor reaches
        // its target (in other words, it will seem like it was never
        // set to hold or brake in the first place).

        System.out.println("\033[2J\nrotateWithStops()\n");
        System.out.println("Rotating 1440 and coasting to a stop.");
        m.setSpeed(720);
        m.coast(); // "seed" stop command
        // Motor will turn until 1440, and then power is removed.
        // Motor will continue to move until it loses momentum,
        // and will not resist further movement.
        m.rotate(1440);
        System.out.println("\nMotor in 'coast' mode.");
        System.out.println("Try to rotate it by hand.\n");
        taskPause();

        System.out.println("\033[2J\nrotateWithStops()\n");
        System.out.println("Rotating 1440 and braking to a stop.");
        m.setSpeed(720);
        m.brake(); // "seed" stop command
        // Motor will turn until 1440, and then power is removed and a
        // passive electrical load is applied to the motor.  It will
        // stop more quickly and be more difficult to turn by hand.
        m.rotate(1440);
        System.out.println("\nMotor in 'brake' mode.");
        System.out.println("Try to rotate it by hand.\n");
        taskPause();


        System.out.println("\033[2J\nrotateWithStops()\n");
        System.out.println("Rotating 1440 and holding at final position.");
        m.setSpeed(720);
        m.hold(); // "seed" stop command
        // Motor will turn until 1440, and then stop suddenly.  The motor
        // will actively resist any attempt to change position from the
        // desired angle.
        m.rotate(1440);
        System.out.println("\nMotor in 'hold' mode.");
        System.out.println("Try to rotate it by hand.\n");
    }


    /**
     * Demonstrate how to run the motor and perform another task while
     * the motor is still executing its turn.
     */
    private static void rotateSimultaneous() {
        System.out.println("\033[2J\nrotateSimultaneous()\n\nRotating 3600 while doing something else\n");

        m.setSpeed(360);
        m.hold(); // seed for a hard stop when the turn is complete

        // By setting the optional second parameter to "true", you are
        // telling rotate() to "immediately return".  This means the
        // program will go on to the next line and continue executing
        // even if the motor hasn't completed turning to the desired
        // location.
        m.rotate(3600, true);

        // If you have two (or more) motors, you could start the next
        // one here by issuing a "rotate()" on it.

        // In order to wait for the motor to stop, we need to set up a
        // loop where we stay trapped until the motor stops moving.
        // If you have more than one motor turning, you can use a
        // logical OR to wait for both to finish, like:
        // while (motorOne.isMoving() || motorTwo.isMoving()) { ... }
        while (m.isMoving()) {
            // Here, the action we're performing at the same time as the
            // motor moving is printing out its angle.  We could also do
            // something unrelated to the motor (such as collecting
            // sensor data, waiting for a button, lighting an LED, etc).
            System.out.printf("\033[0GAngle: %d", m.getTachoCount());

            // You typically want at least a short delay in the loop
            // so you don't spin in the loop doing nothing while you
            // wait for the motor to complete.  If you want to perform
            // some other action more quickly (such as getting a value
            // from a sensor) you can make this smaller.  If you're just
            // printing values on the screen, a longer delay may be
            // more appropriate.
            Delay.msDelay(50);
        }

        // When the loop terminates, m.isMoving() must be false which means
        // the motor has stopped by this line.  You can now start another
        // motor action, or other code as desired.
        System.out.println("\nMotor has stopped");
    }


    /**
     * Demonstrate how to vary motor speed on the fly.
     */
    private static void liveSpeedChange() {
        System.out.println("liveSpeedChange()\n\nRunning motor");
        m.setSpeed(360);

        // Unlike rotate() and rotateTo(), forward() does not need an
        // "immediate return"; it starts the motor running and then
        // immediately goes on to the next line of code (because it
        // has no end condition; it would run forever unless you stop it).
        m.forward();

        System.out.println("\nPress enter to change speed to 100");
        enter.waitForPressAndRelease();

        // Set the new speed
        m.setSpeed(100);
        // The speed doesn't take effect until a new motor command is
        // issued, so we must re-issue forward()
        m.forward();

        System.out.println("Now running at 100\n\nPress enter to stop");
        enter.waitForPressAndRelease();

        m.stop();
    }


} // end class
