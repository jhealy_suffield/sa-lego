import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * <p>
 * A simple TCP client that connects to a listening server and sends
 * input keystrokes to it.
 * </p>
 *
 * @author Jason Healy
 *
 */
public class SampleClient {

	public static void main(String[] args) {
		// attempt to connect to the remote host and port
		try (Socket s = new Socket("::1", 1234)) {
			System.out.println("Connected to remote host "
					+ s.getRemoteSocketAddress());

			System.out.println("You may now send output by typing in this");
			System.out.println("console window.  Press control-D to");
			System.out.println("signal the end of input and quit.");
			System.out.println("Note that sent text may not show up at the");
			System.out.println("remote host until you press enter/return.");

			// get an output stream that sends bytes down the socket
			OutputStream out = s.getOutputStream();
			int b = -1;

			// read bytes from the user's keyboard...
			while ((b = System.in.read()) > -1) {
				// ... and send them down the socket
				out.write(b);
			}
			out.close();
		} catch (IOException ioe) {
			// if an error occurs, let the user know
			System.out.println("Error while writing to remote socket " + ioe);
		}

	}

}
