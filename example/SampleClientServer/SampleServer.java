import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * <p>
 * A simple TCP server that answers connections (one at a time) and
 * prints out any information received from the socket.
 * </p>
 *
 * @author Jason Healy
 *
 */
public class SampleServer {

	/**
	 * We can use this variable to cause the main loop of the program to
	 * terminate.
	 */
	private static boolean listening = true;

	public static void main(String[] args) {
		// Set up a server (listening) socket on a specific port
		ServerSocket ss = null;
		try {
			ss = new ServerSocket(1234);
		} catch (IOException ioe) {
			System.out.println("Unable to create server socket: " + ioe);
		}
		// Answer connections so long as the socket is active and we haven't
		// set "listening" to false
		while (ss != null && listening) {
			// Wait for and accept an incoming connection
			try (Socket s = ss.accept()) {
				System.out.println("Got connection from "
						+ s.getRemoteSocketAddress());

				// get the input from the socket
				InputStream in = s.getInputStream();
				int b = -1;
				// read one byte at a time until the connection is closed
				while ((b = in.read()) > -1) {
					System.out.println("Got input byte: " + b);
				}
				// close our end of the socket
				in.close();
			} catch (IOException ioe) {
				// if an error occurs, let the user know
				System.out.println("Error on socket accept: " + ioe);
			}
		}
	}

}
