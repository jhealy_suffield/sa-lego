package org.suffieldacademy.lego.example;


import ev3dev.sensors.ev3.EV3UltrasonicSensor;

import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;

import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * <p>
 * This program demonstrates the setup, initialization, and data collection
 * from sensors.  Though this example uses an Ultrasonic sensor, the
 * steps work with any type of sensor.
 * </p>
 *
 * @author Jason Healy
 */
public class SensorBasics {

    /** Create a key that we can wait on */
    private static EV3Key enter;


    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // Set the escape key to quit immediately (not related directly
        // to sensors)
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        // Create a key that we can wait on
        enter = new EV3Key(EV3Key.BUTTON_ENTER);


        System.out.println("Initializing sesnsor...");

        // First, you must construct an object that represents the
        // physical sensor on the robot.  In this case, we're using
        // an Ultrasonic sensor on port 1.
        EV3UltrasonicSensor us = new EV3UltrasonicSensor(SensorPort.S1);

        // Many sensors have multiple "modes", where they return
        // different results by measuring different things.  In the
        // case of the Ultrasonic sensor, it can measure single
        // distances, multiple distances, and even communicate with
        // other Ultrasonic sensors.
        //
        // We'll be using the "single distance" mode.
        SampleProvider sp = us.getDistanceMode();

        // No matter what mode you select, it gets assigned to a type
        // named "SampleProvider".  A sample provider is a standard
        // interface that only supports two operations:
        //
        // 1) Determining the "size" of each sample, and
        //
        // 2) Fetching a sample from the sensor.

        // The "size" of the sample is the number of data points
        // in each sample.  For many sensors, the size is 1 as
        // only a single data point comes back.  For example,
        // the Ultrasonic sensor in "distance" mode always returns
        // a single data point (the distance to the closest object).
        //
        // However, other sensors might return multiple values with
        // each sample.  For example, a 3-axis accelerometer would
        // return data points each time: one for x, y, and z.
        //
        // To keep you from having to know/memorize the exact number
        // for each sensor mode, you can ask the SampeProvider for
        // the size and use that to fetch the correct amount of data.
        //
        // The samples are fetched into an array (more on that below),
        // so we can size the array automatically like so:
        float[] data = new float[sp.sampleSize()];

        // All SampleProviders produce a float array for every sample,
        // regardless of the sensor type.  Sensors that return a boolean
        // value should return "0.0" (false) or "1.0" (true).  Most
        // sensors return numeric values in SI units (seconds, meters,
        // degrees, etc).
        //
        // Note that you pass an array as a parameter to the
        // fetchSample() method; you don't assign it using =
        sp.fetchSample(data, 0);

        // At this point, you now have data in the array, and you can do
        // what you want with it.  Frequently, you'll repeat this process
        // in a loop to continue fetching data until you don't want to
        // take samples anymore.
        while (enter.isUp()) {
            sp.fetchSample(data, 0);

            // We only need data[0] because there is only a single
            // data point in each sample.  If there were multiple
            // we would need to access them individually.
            System.out.println("\033[2JHit enter to stop collecting sensor data and quit\n");
            System.out.printf("Ultrasonic Distance (cm):\n%-3.2f\n",
                              data[0]);

            // If you're just trying to display data on the screen, include
            // a short delay so the results stay on the screen long enough
            // to read.  If you're trying to gather samples as quickly as
            // possible (for analysis or other non-display purposes) you
            // can leave this out so you gather them without delay.
            Delay.msDelay(100);
        }

    }


} // end class
