package org.suffieldacademy.lego.example;


import ev3dev.sensors.EV3Key;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;

import lejos.utility.Delay;


/**
 * <p>
 * This program demonstrates how to interact with the buttons (keys)
 * on the EV3.
 * </p>
 *
 * @author Jason Healy
 */
public class ButtonBasics {

    /** Array of characters for gratuitous animation */
    private static final String[] spin = { "/", "-", "\\", "|" };

    /**
     * Initial entry point into the program.
     *
     * @param args Command-line arguments (not used)
     */
    public static void main(String[] args) {
        // counter for animation, don't worry about this
        int si = 0;

        // The buttons on the EV3 are represented as objects, but you
        // must know the "name" of the key to create the object.  See
        // this page for the list of Key names:
        //
        // https://ev3dev-lang-java.github.io/docs/api/latest/ev3dev-lang-java/ev3dev/sensors/EV3Key.KeyType.html

        // Here, we will create an object representing the enter button
        // (the one in the middle of the keypad):
        EV3Key enter = new EV3Key(EV3Key.BUTTON_ENTER);

        // Once you have the object, you can ask it whether is is "down"
        // (pressed) or "up" (not pressed):
        while (enter.isUp()) {
            System.out.println("\033[2JButtonBasics: Up\n");

            System.out.print(spin[si]);
            si = (si + 1) % spin.length;

            System.out.println(" The enter button is up.\nPlease press it down to continue");
            Delay.msDelay(100);
        }

        // At this point the enter button must be down (or else
        // the loop would still be running).  Note that if the user
        // doesn't release the button it might still be down for a "long"
        // time, so if you're going to use the button again you need to
        // wait for them to let go:
        while (enter.isDown()) {
            System.out.println("\033[2JButtonBasics: Down\n");

            System.out.print(spin[si]);
            si = (si + 1) % spin.length;

            System.out.println(" The enter button is down.\nPlease release it to continue");
            Delay.msDelay(100);
        }


        // "Waiting" for a button to be pressed is a very common need,
        // so a button actually "knows" how to wait for a button press.
        //
        // The line below will cause the program to wait until the button
        // is pressed and then released.  Make sure to tell the user what
        // you're waiting for, or they'll think the program is stuck!
        System.out.println("\033[2JButtonBasics\nPress & Release\n");
        System.out.println("Press and release enter to\ncontinue.");
        enter.waitForPressAndRelease();

        // (There is also a "waitForPress()" that continues as soon as
        // the button is pressed, but doesn't wait for the user to let go.)


        // Finally, there is a special construct where you can "register"
        // a piece of code to run whenever a button is pressed.  You can
        // set this up at the beginning of your program, and it will occur
        // any (and every) time the button is pressed, no matter what
        // else the robot is doing at the time.  We usually use it like
        // the example below, which links the code "System.exit()" to the
        // Escape key.  If the user presses this key at any time, the
        // program will quit.  It's a nice way to have a failsafe.
        new EV3Key(EV3Key.BUTTON_ESCAPE).addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(Key k) {
                    System.exit(0);
                }
                @Override
                public void keyReleased(Key k) {
                }
            });

        // Now that the escape button has been registered, we can do
        // anything we want and it can be interrupted by the escape key.
        // Normally you wouldn't want to have an infinite loop like this,
        // but the escape code above will "override" this and force the
        // program to quit.
        while (true) {
            System.out.println("\033[2JButtonBasics\nEvent Handler\n");

            System.out.print(spin[si]);
            si = (si + 1) % spin.length;

            System.out.println(" Press escape to exit");
            Delay.msDelay(100);
        }
    }


} // end class
