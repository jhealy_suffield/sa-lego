# sa-lego

This repository contains code used for the LEGO MINDSTORMS unit in
Suffield Academy's AP CS Principles course.  We use Java as our
implementation language.

In years past, we used the [leJOS](https://lejos.sourceforge.io)
library to program our EV3 bots, but have now moved to the
[ev3dev](https://www.ev3dev.org) build system, which builds on a stock
Debian Linux system and includes a leJOS-like library.

The code in this repository contains a shared Maven POM file that we
use for our programming assignments.  There are several example
folders that demonstrate basic EV3 programming concepts, as well as
several assignment folders intended for students to complete.

## Running The Labs

### One-Time Setup

Before running any of the programs, you must edit the
`lego-settings.xml` file at the root of the repository.  Change the
`<lego.hostname>` declaration to be the hostname of the brick you are
connecting to.  If you've changed the ev3 default username (robot) or
password (maker), change those settings in the file as well.

All the labs will use this setting to connect to your brick and launch
the software.  It is assumed that the brick is powered on, connected
to Wi-Fi, and accepting SSH connections at the hostname or IP address
you provide.  If you need help setting up a brick see the additional
instructions below.


### Running a Lab

Move into the directory of the lab you want to run.

Use Maven to build and deploy the code:

    mvn verify

This will build, upload (via SCP), and run (via SSH) the code.  If the
hostname, username, or password are incorrect, edit
`lego-settings.xml` with the correct values.


## Setting Up an EV3 Brick

This is probably a job for Ansible, but I've written a perl script
that takes a fresh ev3dev image and sets it up for use.  This is very
Suffield-specific, so you only need to do this as part of our official
courses:

1. Install a ev3dev image on a microSD card for the brick.

2. Boot the brick.

3. From the main menu, choose `Wireless and Networks`.

4. Choose `Wi-Fi`.

5. Choose `Powered` and wait a few seconds for the radio to turn on.

6. Choose `Start Scan`.

7. From the list of networks that show up, choose `Suffield Gear`.

8. Choose `Connect`.

9. Enter the password for the network.

10. Choose `Accept`.

11. You should now see `Status: Online` for the network.

12. Press the escape button until you return to the main menu.

13. Now, on your computer, run the `ev3dev-prep` perl script with the hostname/IP address of the brick (or, set the hostname in `lego-settings.xml` and run without arguments):

        ./ev3dev-prep hostname=my-brick-name-or-ip


